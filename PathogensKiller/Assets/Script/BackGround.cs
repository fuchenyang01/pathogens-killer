﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackGround : MonoBehaviour
{
    /*public float speed = 1;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.down * Time.deltaTime * speed);
        //transform.position += (Vector3.down * Time.deltaTime * speed);
        if (transform.position.y < -10.24f)
        {
            transform.position += new Vector3(0, 10.24f * 2, 0);
        }
    }*/
    public float speed = 0.1f;          //Speed of the scrolling

    void Update()
    {
        //Keep looping between 0 and 1
        float y = Mathf.Repeat(Time.time * speed, 1);
        //Create the offset
        Vector2 offset = new Vector2(0, y);
        //Apply the offset to the material
        GetComponent<Renderer>().sharedMaterial.SetTextureOffset("_MainTex", offset);
    }
}
