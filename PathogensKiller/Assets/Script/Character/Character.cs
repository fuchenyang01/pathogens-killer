﻿using UnityEngine;
using System.Collections;

//This script is the base script for both Player and Enemy
//Ensure that the game object this is on has a rigidbody and animator
[RequireComponent(typeof(Rigidbody2D), typeof(Animator))]
public class Character : MonoBehaviour
{
    public float shootDelay = 0.3f;
    public GameObject bulletPrefab;
    public float moveSpeed = 1f;
    public bool canShoot = false;
    public int hp = 1;
    
    public GameObject explosionPrefeb;
    private Transform[] shootPositions;
    public int curHp;
    private void Awake()
    {
        shootPositions = new Transform[this.transform.childCount];
        for (int i = 0; i < shootPositions.Length; i++)
        {
            shootPositions[i] = this.transform.GetChild(i);
        }


    }

    void Explode()
    {
        // Object explode = Instantiate(explosionPrefeb, this.transform.position, this.transform.rotation);
        //Destroy(explode, 0.5f);
        GameObject obj = ObjectPool.current.GetObject(explosionPrefeb);
        //Set its position and rotation
        obj.transform.position = transform.position;
        obj.transform.rotation = transform.rotation;
        //Activate it
        obj.SetActive(true);

    }
    public virtual void OnEnable()
	{
		//If the game is playing and the ship can shoot...
		if (canShoot)
			//...Start it shooting
			StartCoroutine ("Shoot");

        curHp = hp;
	}
    private void Update()
    {
      
    }
    public virtual void OnDisable()
	{
		//If the ship was able to shoot and it became disabled...
		if(canShoot)
			//...Stop shooting
			StopCoroutine ("Shoot");
	}



    //Coroutine
    public IEnumerator Shoot()
    {
        while (true)
        {
            if (GetComponent<AudioSource>())
            {
                GetComponent<AudioSource>().Play();
            }
            foreach (var x in shootPositions)
            {
                //Get a pooled bullet
                GameObject bullet = ObjectPool.current.GetObject(bulletPrefab);
                //Set its position and rotation
                bullet.transform.position = x.position;
                bullet.transform.rotation = x.rotation;
                //Activate it
                bullet.SetActive(true);              
                
            }
            yield return new WaitForSeconds(shootDelay);
        }
    }

    public virtual void OnDie()
    {
        curHp = 0;
        Explode();
        this.gameObject.SetActive(false);
    }

    public void getDamage(int damage)
    {
        curHp -= damage;
        if (curHp <= 0)
        {
            OnDie();
            
        }
        else
        {
            this.GetComponent<SpriteRenderer>().color = Color.red;
            StartCoroutine("SetColor");
        }

    }

    IEnumerator SetColor()
    {
        yield return new WaitForSeconds(0.1f);
        this.GetComponent<SpriteRenderer>().color = Color.white;
    }

    public void SetShootposition()
    {
        shootPositions = new Transform[this.transform.childCount];
        for (int i = 0; i < shootPositions.Length; i++)
        {
            shootPositions[i] = this.transform.GetChild(i);
        }
    }
}