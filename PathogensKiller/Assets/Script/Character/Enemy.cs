﻿using UnityEngine;
using System.Collections;
using System;

//This script controls the enemy ships
public class Enemy : Character
{
    private Vector3 originPosition;
    private Quaternion originRotation;
    public GameObject Coin;
    public int Score = 100;


    private void FixedUpdate()
    {
       
            Transform player = GameObject.Find("Player").transform;
            float angle = Vector3.Angle(-transform.up, player.position - transform.position);
            Vector3 v = Vector3.Cross(-transform.up, player.position - transform.position);
            if (v.z > 0)
            {
                transform.Rotate(Vector3.forward * angle * Time.deltaTime);

            }
            else
            {
                transform.Rotate(Vector3.back * angle * Time.deltaTime);
            }
            transform.rotation = new Quaternion(transform.rotation.x, transform.rotation.y, -transform.rotation.z, -transform.rotation.w);

      
    }
    
    public override void OnDisable()
    {
        base.OnDisable();
        
        this.transform.position = originPosition;
        this.transform.rotation = originRotation;
    }

    public override void OnEnable()
    {
        base.OnEnable();
        originPosition = this.transform.position;
        originRotation = this.transform.rotation;
        this.GetComponent<Rigidbody2D>().velocity = -this.transform.up * moveSpeed;

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.layer == LayerMask.NameToLayer("Bullet(Player)"))
        {           
            getDamage(collision.gameObject.GetComponent<Bullet>().power);
            ObjectPool.current.PoolObject(collision.gameObject);
        }
        else if (collision.gameObject.tag=="Laser")
        {
            this.OnDie();
        }
        else if (collision.gameObject.layer == LayerMask.NameToLayer("Player"))
        {
            if (collision.gameObject.GetComponent<Shield>()!=null)
            {
                this.OnDie();
                collision.gameObject.GetComponent<Shield>().getDamage(1);
            }
            else if (collision.gameObject.tag == "Player")
            {
                collision.gameObject.GetComponent<Player>().OnDie();
                this.OnDie();
            }
           
        }
       
    }

   

    public override void OnDie()
    {
        GameObject obj = ObjectPool.current.GetObject(Coin);
        //Set its position and rotation
        obj.transform.position = transform.position;
        obj.transform.rotation = transform.rotation;
        //Activate it
        obj.SetActive(true);
        base.OnDie();
        Manager.current.AddPoint(Score);
        if (Ultimate.current.isUltimate==false)
        {
            Ultimate.current.AddEnergyValue(Score);
        }

    }
}