﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;

public class Player : Character
{
    
    private float PlayerWidth;
    private float PlayerHeight;
    private bool isMouseDown = false;
    private Vector3 lastMousePosition = Vector3.zero;
    public bool isMagnet = false;

    public AudioClip coin;
    public Slider Sld_HP;
    private AudioSource source;   


    private void Start()
    {
        PlayerWidth = (this.GetComponent<BoxCollider2D>().size.x * this.transform.localScale.x) / 2;
        PlayerHeight = (this.GetComponent<BoxCollider2D>().size.y * this.transform.localScale.y) / 2;

        source = GetComponent<AudioSource>();
    }
    void Update()
    {

        Sld_HP.value = (float)curHp/ (float)hp;
        Move();

        if (isMagnet)
        {
            
            GameObject[] coins = GameObject.FindGameObjectsWithTag("Coin") as GameObject[];
          
            foreach (var coin in coins)
            {
                coin.GetComponent<Coin>().isMove = true;               
            }

        }
        else if (isMagnet==false)
        {
            GameObject[] coins = GameObject.FindGameObjectsWithTag("Coin") as GameObject[];

            foreach (var coin in coins)
            {
                coin.GetComponent<Coin>().isMove = false;
            }
        }



    }

    void Move()
    {
        Vector2 min = Camera.main.ViewportToWorldPoint(new Vector2(0, 0));
        Vector2 max = Camera.main.ViewportToWorldPoint(new Vector2(1, 1));
        Vector2 pos = transform.position;
        if (Input.GetMouseButtonDown(0))
        {
            isMouseDown = true;
        }
        if (Input.GetMouseButtonUp(0))
        {
            isMouseDown = false;
            lastMousePosition = Vector3.zero;
        }
        if (isMouseDown)
        {
            if (lastMousePosition != Vector3.zero)
            {
                Vector2 offset = Camera.main.ScreenToWorldPoint(Input.mousePosition) - lastMousePosition;
                pos += offset*moveSpeed;
            }
            lastMousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        }

        pos.x = Mathf.Clamp(pos.x, min.x + PlayerWidth, max.x - PlayerWidth);
        pos.y = Mathf.Clamp(pos.y, min.y + PlayerHeight, max.y - PlayerHeight);
        transform.position = pos;
     
    }

    public override void OnDie()
    {
        base.OnDie();
        Manager.current.GameOver();
        SceneManager.LoadScene(0);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (GameObject.Find("shield(Clone)")==null)
        {
            if (collision.gameObject.layer == LayerMask.NameToLayer("Bullet(Enemy)"))
            {
                getDamage(collision.gameObject.GetComponent<Bullet>().power);
                ObjectPool.current.PoolObject(collision.gameObject);
            }
            else if (collision.gameObject.layer == LayerMask.NameToLayer("Stone"))
            {
                this.OnDie();
            }
        }
        

         if (collision.gameObject.layer == LayerMask.NameToLayer("Item"))
        {
            ObjectPool.current.PoolObject(collision.gameObject);
            collision.gameObject.GetComponent<Item>().UseItem();          
            source.PlayOneShot(collision.gameObject.GetComponent<Item>().collect, 1F);
        }
        else if (collision.gameObject.layer == LayerMask.NameToLayer("Coin"))
        {
           ObjectPool.current.PoolObject(collision.gameObject);
            source.PlayOneShot(coin, 1F);
            Manager.current.SetBalance(1);

        }
        
    }

   
}
