﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
    GameObject target;
   
    public bool isMove = false;

    public float speed = 10;

    private void Start()
    {
        target = GameObject.FindGameObjectWithTag("Player");
    }
    void Update()
    {
        
        if (isMove)
        {
            //move to Player
            // transform.position = Vector3.Lerp(transform.position, target.position, 0.2f);
            if (target!=null)
            {
                transform.position = Vector3.MoveTowards(transform.position, target.transform.position, Time.deltaTime * speed);
            }
           
        
        }
    }

}
