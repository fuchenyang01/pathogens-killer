﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyArea : MonoBehaviour
{
    public Player player;
   
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag=="Enemy")
        {
            collision.gameObject.SetActive(false);
            player.getDamage(1);
            this.gameObject.GetComponent<MeshRenderer>().material.color = Color.red;
            StartCoroutine("SetColor");
        }
        else if(collision.gameObject.GetComponent<Bullet>() != null)
        {
            ObjectPool.current.PoolObject(collision.gameObject);
        }
        else if (collision.gameObject.layer == LayerMask.NameToLayer("Item"))
        {
            ObjectPool.current.PoolObject(collision.gameObject);
        }
        else if (collision.gameObject.layer == LayerMask.NameToLayer("Coin"))
        {
            ObjectPool.current.PoolObject(collision.gameObject);
        }
        else if (collision.gameObject.layer == LayerMask.NameToLayer("Stone"))
        {
            ObjectPool.current.PoolObject(collision.gameObject);
        }
    }

    IEnumerator SetColor()
    {
        yield return new WaitForSeconds(0.1f);
        this.gameObject.GetComponent<MeshRenderer>().material.color = Color.white;
    }
}
