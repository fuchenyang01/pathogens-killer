﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Emitter : MonoBehaviour
{
    public GameObject[] wavePrefabs;
    private int curWave = 0;
    private GameObject[] waves;
    // Start is called before the first frame update
    void Start()
    {
        waves = new GameObject[wavePrefabs.Length];

        for (int i = 0; i < wavePrefabs.Length; i++)
        {
            GameObject wave = (GameObject)Instantiate(wavePrefabs[i], this.transform.position,Quaternion.identity);
            wave.transform.parent = this.transform;
            wave.SetActive(false);

            waves[i] = wave;
        }
        StartCoroutine(EmitWave());
    }

    IEnumerator EmitWave()
    {
        while (true)
        {
            WaveManager w = waves[curWave].GetComponent<WaveManager>();

            waves[curWave].SetActive(true);
            while (w.HasAliveShip())
            {
                yield return new WaitForEndOfFrame();
            }
            //create new Wave
            waves[curWave].SetActive(false);
            curWave++;
            if (curWave== waves.Length)
            {
                curWave = 0;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
