﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour
{
    public float lifeTime = 0.5f;     //Lifetime of the explosion in seconds


    void OnEnable()
    {
        //Invoke the Die method
        Invoke("Die", lifeTime);
    }

    void OnDisable()
    {
        //Cancel the invoke if something else removes the explosion
        CancelInvoke("Die");
    }

    void Die()
    {
        //Re-add the explosion to the pool
        ObjectPool.current.PoolObject(gameObject);
    }
}
