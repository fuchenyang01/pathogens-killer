﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Generator : MonoBehaviour
{
    //Instantiated position
    public Transform GeneratLocation;

    public GameObject Item_upgrade;
    public GameObject StonePrefab;
    public GameObject Item_bomb;
    public GameObject Item_shield;
    public GameObject Item_Magnet;
    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("CreateItem_upgrade", 10f, 30f);
        InvokeRepeating("CreateItem_bomb", 20f, 28f);
        InvokeRepeating("CreateItem_shield", 15f,28f);
        InvokeRepeating("CreateItem_Magnet", 18f, 25f);
        InvokeRepeating("Create_Stone", 10f, 15f);
    }

    // Update is called once per frame

    void CreateItem_upgrade()
    {
        GameObject upgrade = ObjectPool.current.GetObject(Item_upgrade);
        //Set its position and rotation
        upgrade.transform.position = new Vector3(Random.Range(-2.4f, 2.4f), GeneratLocation.position.y + 1.5f, 0f);
        upgrade.transform.rotation = Item_upgrade.transform.rotation;
        //Activate it
        upgrade.SetActive(true);
    }

    void Create_Stone()
    {
        GameObject Stone = ObjectPool.current.GetObject(StonePrefab);
        //Set its position and rotation
        Stone.transform.position = new Vector3(Random.Range(-2.4f, 2.4f), GeneratLocation.position.y + 5.5f, 0f);
        Stone.transform.rotation = Stone.transform.rotation;
        //Activate it
        Stone.SetActive(true);
    }

    void CreateItem_bomb()
    {
        GameObject bomb = ObjectPool.current.GetObject(Item_bomb);
        //Set its position and rotation
        bomb.transform.position = new Vector3(Random.Range(-2.4f, 2.4f), GeneratLocation.position.y + 2.5f, 0f);
        bomb.transform.rotation = Item_bomb.transform.rotation;
        //Activate it
        bomb.SetActive(true);
    }

    void CreateItem_shield()
    {
        GameObject shield = ObjectPool.current.GetObject(Item_shield);
        //Set its position and rotation
        shield.transform.position = new Vector3(Random.Range(-2.4f, 2.4f), GeneratLocation.position.y + 3.5f, 0f);
        shield.transform.rotation = Item_shield.transform.rotation;
        //Activate it
        shield.SetActive(true);
    }

    void CreateItem_Magnet()
    {
        GameObject Magnet = ObjectPool.current.GetObject(Item_Magnet);
        //Set its position and rotation
        Magnet.transform.position = new Vector3(Random.Range(-2.4f, 2.4f), GeneratLocation.position.y + 4.5f, 0f);
        Magnet.transform.rotation = Item_shield.transform.rotation;
        //Activate it
        Magnet.SetActive(true);
    }

}
