﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Item : MonoBehaviour
{
    public float speed = 1.5f;
    public GameObject shield;
    private GameObject shieldObj;
    private GameObject player;
    private float MagnetTimer = 5;
    //private Vector3 oriShootPosition;
    public AudioClip collect;   
    public enum ItemType
    {
        Item_bomb,
        Item_shield,
        Item_upgrad,
        //Item_upgrad2,
        Item_Magnet
    }
    public ItemType m_ItemType;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player");
        
    }

    // Update is called once per frame
    void Update()
    {
        player = GameObject.Find("Player");
        this.transform.Translate(Vector3.down * speed * Time.deltaTime);
        if (player!=null)
        {
            if (player.GetComponent<Player>().isMagnet == true)
            {
                MagnetTimer -= Time.deltaTime;
                if (MagnetTimer <= 0)
                {
                    player.GetComponent<Player>().isMagnet = false;
                    MagnetTimer = 5;

                }
            }
        }
        
        
    }

    public void UseItem()
    {
        switch (m_ItemType)
        {
            case ItemType.Item_bomb:
                
                Item_bomb();
                break;
            case ItemType.Item_shield:
               
                Item_shield();
                break;
            case ItemType.Item_upgrad:
                
                Item_upgrad();
                break;

          /* case ItemType.Item_upgrad2:

                Item_upgrad2();
                break;*/
            case ItemType.Item_Magnet:
                
                Item_Magnet();
                break;
            default:
                break;
        }
    }

    public void Item_shield()
    {
        
        player.tag = "Shield";
        shieldObj = Instantiate(shield, player.transform.position, player.transform.rotation);
        shieldObj.transform.SetParent(player.transform);
        
    }

    public void Item_upgrad()
    {
        
        player.GetComponent<Player>().shootDelay /= 1.1f;
    }
    int i = 0;
    /*public void Item_upgrad2()
    {
        i++;
        oriShootPosition = player.transform.position + player.transform.up/2 ;
        Vector3 offset = new Vector3(0.1f, 0, 0);
        Vector3 shootPosition1 = oriShootPosition + offset * i;
        Vector3 shootPosition2 = oriShootPosition - offset * i;
        
       // GameObject obj1 = Instantiate(oriShootPosition, shootPosition1, player.transform.rotation);
        //GameObject obj2 = Instantiate(oriShootPosition, shootPosition2, player.transform.rotation);
        GameObject obj1 = new GameObject("Shootposition");
        GameObject obj2 = new GameObject("Shootposition");

       
        obj1.transform.position = shootPosition1;
        obj1.transform.rotation = player.transform.rotation;
        obj1.transform.SetParent(player.transform);
        obj2.transform.position = shootPosition2;
        obj2.transform.rotation = player.transform.rotation;
        obj2.transform.SetParent(player.transform);
        player.GetComponent<Player>().SetShootposition();
    }*/

    public void Item_bomb()
    {
        
        GameObject[] all = FindObjectsOfType(typeof(GameObject)) as GameObject[];
        foreach (GameObject wave in all)    
        {
            Debug.Log(wave.gameObject.name); 
            if (wave.gameObject.tag == "wave")
            {
                Transform[] enemys= new Transform[wave.transform.childCount];
                for (int i = 0; i < wave.transform.childCount; i++)
                {
                    enemys[i] = wave.transform.GetChild(i);
                    enemys[i].GetComponent<Enemy>().OnDie();
                }               
                
            }
           
            
        }


    }

    public void Item_Magnet()
    {
     
        player.GetComponent<Player>().isMagnet = true;
    }
}
