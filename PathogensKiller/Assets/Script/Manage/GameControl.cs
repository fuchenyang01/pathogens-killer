﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class GameControl : MonoBehaviour
{
    public GameObject TurnOff;
    public GameObject TurnOn;
    public GameObject PlayingPanel;
    public GameObject PausePanel; 
    public AudioSource BackgroudMusic;
    // Start is called before the first frame update
    void Start()
    {

        BackgroudMusic = this.GetComponent<AudioSource>();
        BackgroudMusic.loop = true;
        BackgroudMusic.volume = 1f;
        BackgroudMusic.Play();
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    
    // Buttons in the exit dialog.
    public void Btn_ExitYes()
    {
        // Quit the game.
        Application.Quit();
        Debug.Log("Exit");
    }

    
    //The following are the button of playing.
    public void Btn_Pause()
    {
        //Pause the game while the player is playing.
        Time.timeScale = 0;
        PausePanel.SetActive(true);
        BackgroudMusic.Pause();
    }
    public void Btn_Continue()
    {
        //Continue the game.
        Time.timeScale = 1;
        PausePanel.SetActive(false);
        BackgroudMusic.Play();
    }

    public void Btn_Replay()
    {
        SceneManager.LoadScene(1);
        Time.timeScale = 1;
    }

    public void Btn_Return()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(0);
        PlayingPanel.SetActive(false);
    }
   
    //The following are the button of music control.
    public void Btn_TurnOff()
    {
        Debug.Log("trunoff");
        //Turn off music.
        TurnOn.SetActive(true);
        TurnOff.SetActive(false);
        BackgroudMusic = this.GetComponent<AudioSource>();


        BackgroudMusic.volume = 0f;
        BackgroudMusic.loop = false;
       
    }

    public void Btn_TurnOn()
    {
        Debug.Log("turn on");
        //Turn on music.
        TurnOff.SetActive(true);
        TurnOn.SetActive(false);
        BackgroudMusic = this.GetComponent<AudioSource>();       

        BackgroudMusic.volume = 1f;
        BackgroudMusic.loop = true;
       
    }
   
  

}
