﻿using UnityEngine;
using UnityEngine.UI;

//This is the manager for the game
public class Manager : MonoBehaviour
{
	public static Manager current;			//A public static reference to itself (make's it visible to other objects without a reference)
	public GameObject player;				//The player ship
	public GameObject titleObject;			//The game object containing the title text
	public Text scoreText;			//The score text
	public Text highScoreText;		//The high score text
    public Text Coins;
	public int Balance = 0;
    int score;								//The player's score
	int highScore;							//The high score
	string highScoreKey = "highScore";		//Name of the high score
    string CoinKey = "Coin";

    void Awake()
	{
       
        //Ensure that there is only one manager
        if (current == null)
			current = this;
		else
			Destroy (gameObject);
	}

	void Start ()
	{
		Initialize ();
        GameStart();
    }

	void Update ()
	{

		//if the player beats the high score, the high score is set to their score
		if (highScore < score) {
			highScore = score;
		}

		//Set the GUI to relfect the current score and high score
		scoreText.text = "Score : " + score.ToString ();
        highScoreText.text = "HighScore : " + highScore.ToString ();
        Coins.text = Balance.ToString();
	}
	
	void GameStart ()
	{
        //Deactivate the title and activate the player
        titleObject.SetActive (false);
        player.SetActive(true);
       // Instantiate(player, this.transform.position, this.transform.rotation);
	}
	
	public void GameOver ()
	{

        //Call the save method
        Save();
		//Activate the title
		titleObject.SetActive (true);
	}
	
	public bool IsPlaying ()
	{
		//if the title is active, then the player isn't playing
		return player.activeSelf == true;
	}

	private void Initialize ()
	{
		//Reset the score and get the high score from the playerprefs
		score = 0;
		highScore = PlayerPrefs.GetInt (highScoreKey, 0);
        Balance= PlayerPrefs.GetInt(CoinKey, 0);
    }
	
	public void AddPoint (int point)
	{
		//Add points to the player's score
		score += point;
	}
	
	public void Save ()
	{
		//Save the highscore to the player prefs
		PlayerPrefs.SetInt (highScoreKey, highScore);
        PlayerPrefs.SetInt(CoinKey, Balance);
        PlayerPrefs.Save ();
		//Re initialize the score
		Initialize ();
	}

    public void SetBalance(int v)
    {
        Balance += v;
    }
}