﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuControl : MonoBehaviour
{
    public GameObject HelpPanel;
    public GameObject LeaderboardPanel;
    public GameObject ExitDialog;
    public GameObject TurnOff;
    public GameObject TurnOn;
    public GameObject MainMenuPanel;
    public Text HighScore;
    public Text Balance;
    
    public AudioSource BackgroudMusic;
    // Start is called before the first frame update
    void Start()
    {

        BackgroudMusic = this.GetComponent<AudioSource>();
        BackgroudMusic.loop = true;
        BackgroudMusic.volume = 1f;
       

        BackgroudMusic.Play();
    }

    // Update is called once per frame
    void Update()
    {
        HighScore.text = "HighScore: " + PlayerPrefs.GetInt("highScore", 0).ToString();
        Balance.text =  PlayerPrefs.GetInt("Coin", 0).ToString();
    }

    //The following are the button of menu.
    public void Btn_Start()
    {
        //Start the game.
        Time.timeScale = 1;
        SceneManager.LoadScene(1);
        BackgroudMusic.volume = 1f;
        BackgroudMusic.Play();
        MainMenuPanel.SetActive(false);
    }
    public void Btn_Leaderboard()
    {
        //Show leaderboard.
        LeaderboardPanel.SetActive(true);
    }
    public void Btn_Help()
    {
        //Show a help dialog.
        HelpPanel.SetActive(true);
    }

    public void Btn_Exit()
    {
        //Show Dialog.
        ExitDialog.SetActive(true);
    }

    // Buttons in the exit dialog.
    public void Btn_ExitYes()
    {
        // Quit the game.
        Application.Quit();
        Debug.Log("Exit");
    }

    public void Btn_ExitCancle()
    {
        ExitDialog.SetActive(false);
    }

    //The following are the button of playing.
   
   

    public void Btn_Replay()
    {
        SceneManager.LoadScene(1);
        Time.timeScale = 1;
    }

   
    //The following are the button of music control.
    public void Btn_TurnOff()
    {
        Debug.Log("trunoff");
        //Turn off music.
        TurnOn.SetActive(true);
        TurnOff.SetActive(false);
        BackgroudMusic = this.GetComponent<AudioSource>();


        BackgroudMusic.volume = 0f;
        BackgroudMusic.loop = false;

    }

    public void Btn_TurnOn()
    {
        Debug.Log("turn on");
        //Turn on music.
        TurnOff.SetActive(true);
        TurnOn.SetActive(false);
        BackgroudMusic = this.GetComponent<AudioSource>();
        


        BackgroudMusic.volume = 1f;
        BackgroudMusic.loop = true;

    }    

}
