﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveManager : MonoBehaviour
{
    public GameObject[] enemise;


    private void OnEnable()
    {
        for (int i = 0; i < enemise.Length; i++)
        {
            enemise[i].SetActive(true);
        }
    }


    private void OnDisable()
    {
        for (int i = 0; i < enemise.Length; i++)
        {
            enemise[i].SetActive(false);
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        enemise = new GameObject[transform.childCount];
        for (int i = 0; i < transform.childCount; i++)
        {
            enemise[i] = transform.GetChild(i).gameObject;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    internal bool HasAliveShip()
    {
        //The enemy is destroyed or leaves the background area and thinks the death
        for (int i = 0; i < transform.childCount; i++)
        {
            if (transform.GetChild(i).gameObject.activeSelf)
            {
                return true;
            }
        }
        return false;
    }
}
