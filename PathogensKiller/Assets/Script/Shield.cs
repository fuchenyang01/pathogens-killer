﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shield : MonoBehaviour
{
    public float lifeTime = 10;
    public float HP = 10;
    // Start is called before the first frame update
    void OnEnable()
    {
        //Invoke the Die method
        Invoke("Die", lifeTime);
    }

    void OnDisable()
    {
        //Stop the Die method (in case something else put this bullet back in the pool)
        CancelInvoke("Die");
    }

    void Die()
    {
        Destroy(this.gameObject);
        GameObject player = GameObject.Find("Player");
        player.tag = "Player";
    }

    public void getDamage(int damage)
    {
        HP -= damage;
        if (HP <= 0)
        {
            Die();            
        }
        else
        {
            this.GetComponent<SpriteRenderer>().color = Color.red;
            StartCoroutine("SetColor");
        }

    }

    IEnumerator SetColor()
    {
        yield return new WaitForSeconds(0.1f);
        this.GetComponent<SpriteRenderer>().color = Color.white;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Bullet(Enemy)"))
        {
            getDamage(collision.gameObject.GetComponent<Bullet>().power);
            ObjectPool.current.PoolObject(collision.gameObject);
        }
    }
 }
