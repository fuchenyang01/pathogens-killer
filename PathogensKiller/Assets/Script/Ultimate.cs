﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Ultimate : MonoBehaviour
{
    float energyValue = 0;
    public static Ultimate current;
    public Slider Energy;
    public Button ultimate;
    public GameObject Laser;
    public bool isUltimate = false;

    // Start is called before the first frame update
    void Start()
    {
        if (current == null)
            current = this;
    }

    // Update is called once per frame
    void Update()
    {
        Energy.value = energyValue / 10000;
        if (isUltimate==false)
        {
            if (Energy.value >= 1)
            {
                ultimate.gameObject.SetActive(true);
            }
        }
       
    }
    public void AddEnergyValue(float value)
    {
        energyValue += value;
    }

    public void OnUltimateClick()
    {
        isUltimate = true;
        Laser.SetActive(true);
        ultimate.gameObject.SetActive(false);
        energyValue = 0;
        StartCoroutine("CloseLaser");
    }

    IEnumerator CloseLaser()
    {
        yield return new WaitForSeconds(5f);
        isUltimate = false;
        Laser.SetActive(false);
        
    }
}
